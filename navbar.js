function nav() {
    var navIcon = document.querySelector(".nav-icon");
    navIcon.addEventListener("click", function () {
        var subMenu = document.querySelector(".sub-menu");
        if ([...subMenu.classList].includes("active")) {
            subMenu.classList.remove("active");
            return false
        }
        subMenu.classList.add("active");
    })
}
nav()